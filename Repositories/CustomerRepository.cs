﻿using dotnetcore.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dotnetcore.Repositories;

namespace dotnetcore.Repositories
{
    
    public class CustomerRepository : ICustomerRepository
    {   
        /* Customer Repository, implements ICustomerRepository interface. 
         * Stores all methods regarding customer. 
         * 
        */
        
        //Assignment task 1.
        //Uses @range1 and @range2 to GET customers with ID range. Returns list of Customer
        public List<Customer> GetSpecificCustomers(string range1, string range2)
        {
            List<Customer> customerList = new List<Customer>();

            string sql1 = "SELECT CustomerId, Firstname, Lastname, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE CustomerId BETWEEN @range1 AND @range2";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        cmd.Parameters.AddWithValue("@range1", range1);
                        cmd.Parameters.AddWithValue("@range2", range2);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.Phone = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.PostalCode = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }

                }

            }
            catch (SqlException ex)
            {

            }
            return customerList;
        }
        //Assignment task 2.
        //GET all customers from database, returns list of customer
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql1 = "SELECT CustomerId, Firstname, Lastname, Country, PostalCode, Phone, Email " +
                "FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.Phone = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.PostalCode = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }

                }

            }
            catch (SqlException ex)
            {

            }
            return customerList;
        }
        //Assignment task 3. 
        // Select a given customerID and returns given parameters.
        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();
            string sql1 = "SELECT CustomerId, Firstname, Lastname, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE CustomerId = @CustomerId ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.Phone = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(4) ? null : reader.GetString(6);
                            }
                        }
                    }

                }
            }
            catch (SqlException ex)
            {

            }
            return customer;
        }
        //Assignment task 4. 
        // Input a first name and last name and returns given paramters for that specific customer.
        public Customer GetCustomerByName(string firstName, string lastName)
        {
            Customer customer = new Customer();
            string sql1 = "SELECT CustomerId, Firstname, Lastname, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE Firstname = @Firstname AND Lastname = @Lastname";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        cmd.Parameters.AddWithValue("@Firstname", firstName);
                        cmd.Parameters.AddWithValue("@lastName", lastName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.Phone = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(4) ? null : reader.GetString(6);
                            }
                        }
                    }

                }
            }
            catch (SqlException ex)
            {

            }
            return customer;
        }
        //Assignment task 5.
        // Adds a new customer to the database with given paramters.
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, " +
                "PostalCode, Phone, Email) VALUES " +
                "(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return success;

        }
        //Assignment task 6.
        //Update the customer matching the customerId that is inputed with the paramters given.
        public bool UpdateCustomer(Customer customer, int id)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, " +
                "Country = @Country, Phone = @Phone," +
                " PostalCode = @PostalCode, Email = @Email WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
               
            }
            return success;




        }
        //Ässignment task 7
        //Returns the amount of customers from different countires. 
        public List<CustomerCountry> GetNumberInnEachCountry()
        {

            List<CustomerCountry> NumbInCountry = new List<CustomerCountry>();
            string sql1 = "SELECT country, COUNT(*) FROM Customer Group By country Order By COUNT(*) DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                temp.NumberOf = reader.GetInt32(1);
                                temp.Country = reader.GetString(0);
                                NumbInCountry.Add(temp);

                            }
                        }
                    }

                }

            }
            catch (SqlException ex)
            {
            }return NumbInCountry;
        }
        //Assignment task 8
        //Check the invoic total each customer has spent and returns their name with the matching total.
        public List<CustomerSpender> GetBiggestSpender()
        {
            List<CustomerSpender> biggestSpender = new List<CustomerSpender>();
            string sql1 = "SELECT Customer.FirstName, Customer.LastName, Invoice.Total FROM Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "Group By Customer.FirstName, Customer.LastName, Invoice.Total Order By Invoice.Total DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.FirstName = reader.GetString(0);
                                temp.LastName = reader.GetString(1);
                                temp.Total = reader.GetDecimal(2);
                                biggestSpender.Add(temp);

                            }
                        }
                    }

                }

            }
            catch (SqlException ex)
            {
            }
            return biggestSpender;
        }
        //Assignment task 9.
        //Check what genre is most popular for the given customerId returns the ID and the genre name.
        public List<CustomerGenre> GetWhatGenre(int id)
        {
            List<CustomerGenre> whatGenre = new List<CustomerGenre>();
            string sql1 = "SELECT TOP 1 with ties G.Name, I.CustomerId, Count(G.Name) as amount FROM Invoice I " +
                "INNER JOIN InvoiceLine IL ON IL.InvoiceId = I.InvoiceId INNER JOIN Track T ON IL.TrackId = T.TrackId " +
                "INNER JOIN Genre G ON T.GenreId = G.GenreId Where I.CustomerId = @CustomerId GROUP BY I.CustomerId, G.Name " +
                "ORDER BY amount DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.getConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql1, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(2);
                                temp.Name = reader.GetString(0);
                                whatGenre.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException ex)
            {

            }
            return whatGenre;
        }
    }
}
    
