﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dotnetcore.Models;

namespace dotnetcore.Repositories
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();
        public Customer GetCustomer(string id);
        public Customer GetCustomerByName(string firstName, string lastName);
        public List<Customer> GetSpecificCustomers(string from, string too);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer, int CustomerId);
        public List<CustomerCountry> GetNumberInnEachCountry();
        public List<CustomerSpender> GetBiggestSpender();
        public List<CustomerGenre> GetWhatGenre(int id);

    }

}
