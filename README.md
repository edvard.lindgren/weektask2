# Assignment 2: C# Data acces with SQL client

Second Assignment in Noroff accelerate .net course, made by Edvard Lindgren and Martin Vottestad Stenberg.
Teknologies used in this assignment. 

- .net / C#
- Microsoft SQL server
- Microsoft SQL server Managment Studio


### Goal
Write SQL scripts and build a console application in C#

## Apendix 1


Directory "SQL scripts Apendix1" contains all SQL scripts for Apendix 1 SQL scripts. 

## Apendix 2

Directory's "Models" and "Repositories". 
Some files contains multiple tasks. Each different tasked is marked with a comment. 

In this assignment we used the provided Chinook database script. 
This database consists of data regarding a "totly legit not nockoff" version of Itunes. 
Goal off this task is to use the SQLclient library to accesess, showcase and update data from the database in C#

In this task we used multiple model classes that inherited their data from the parent customer class and added necessary extra variables.

The repository contains all functions regarding database accesesing, database connections and interface

