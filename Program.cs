﻿using System;
using System.IO;
using Microsoft.Data.SqlClient;
using System.Data.SqlClient;
using dotnetcore.Models;
using dotnetcore.Repositories;
using System.Collections.Generic;

namespace dotnetcore
{
   public class Program
    { 

        static void Main(string[] args)
        {

            //Print function for the Customer class
            static void PrintCustomer(Customer customer)
            {
                Console.WriteLine($"--- { customer.CustomerId} {customer.FirstName} {customer.LastName}" +
                    $" {customer.Country} {customer.Phone} {customer.PostalCode} {customer.Email} ---");
            }
            //Print function for the CustomerCountry class.
            static void PrintCustomerCountry(CustomerCountry customer)
            {
                Console.WriteLine($"--- { customer.NumberOf} {customer.Country} ---");
            }
            //Print function for the CustomerSpender class.
            static void PrintCustomerSpender(CustomerSpender customer)
            {
                Console.WriteLine($"--- {customer.FirstName} {customer.LastName}" +
                    $" {customer.Total} {customer.BiggestSpender} --- ");
            }
            //Print function for the CustomerGenre class.
            static void PrintCustomerGenre(CustomerGenre customer)
            {
                Console.WriteLine($"--- {customer.CustomerId} {customer.Name}" +
                    $"  --- ");
            }

            //Print function for multiple Customers
            static void PrintCustomers(IEnumerable<Customer> customers)
            {
                foreach (Customer customer in customers)
                {
                    PrintCustomer(customer);
                }
            }
            //Print function for multiple CustomersCountry
            static void PrintCustomersCountry(IEnumerable<CustomerCountry> customers)
            {
                foreach (CustomerCountry customer in customers)
                {
                    PrintCustomerCountry(customer);
                }
            }
            //Print function for multiple CustomersSpenders
            static void PrintCustomersSpender(IEnumerable<CustomerSpender> customers)
            {
                foreach (CustomerSpender customer in customers)
                {
                    PrintCustomerSpender(customer);
                }
            }
            //Print function for multiple CustomersGenre
            static void PrintCustomersGenre(IEnumerable<CustomerGenre> customers)
            {
                foreach (CustomerGenre customer in customers)
                {
                    PrintCustomerGenre(customer);
                }
            }

        }
    }
    
}
