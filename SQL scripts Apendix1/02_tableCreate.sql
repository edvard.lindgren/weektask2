CREATE TABLE Superhero(
	ID int IDENTITY(1,1) PRIMARY KEY,
	Realname nvarchar(255),
	Alias nvarchar(255),
	Origin nvarchar(255)
	)

	CREATE TABLE Assistent (
	ID int IDENTITY(1,1) PRIMARY KEY,
	RealName nvarchar(255)
	)

	CREATE TABLE SuperPower (
	ID int IDENTITY(1,1) PRIMARY KEY,
	DescriptionLong nvarchar(255)
	)