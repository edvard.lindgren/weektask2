create table SuperHero_SuperPower_junction
(
  SuperHero_id int,
  SuperPower_id int,
  CONSTRAINT SuperHero_power_pk PRIMARY KEY (SuperHero_id, SuperPower_id),
  CONSTRAINT FK_Superhero 
      FOREIGN KEY (SuperHero_id) REFERENCES Superhero (ID),
  CONSTRAINT FK_SuperPower 
      FOREIGN KEY (SuperPower_id) REFERENCES SuperPower(ID)
);