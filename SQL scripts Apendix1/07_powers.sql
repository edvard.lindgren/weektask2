INSERT INTO SuperPower(DescriptionLong)
	values
	('Super Strength'),
	('Invisbility'),
	('Flying'),
	('Rich');

	INSERT INTO SuperHero_SuperPower_junction(SuperHero_id,SuperPower_id)
	VALUES
	((SELECT ID FROM Superhero Where Alias = 'Batman'),(SELECT ID FROM SuperPower Where DescriptionLong = 'Rich')),
	((SELECT ID FROM Superhero Where Alias = 'Superman'),(SELECT ID FROM SuperPower Where DescriptionLong = 'Super Strength')),
	((SELECT ID FROM Superhero Where Alias = 'Superman'),(SELECT ID FROM SuperPower Where DescriptionLong = 'Flying')),
	((SELECT ID FROM Superhero Where Alias = 'Arrow'),(SELECT ID FROM SuperPower Where DescriptionLong = 'Rich'))