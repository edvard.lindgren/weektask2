﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetcore.Models
{
    public class CustomerSpender : Customer
    {
        /*CustomerSpender inherits from customer
         * Stores all variables regarding CustomerSpender
         */
        public decimal Total { get; set; }
    }
}
