﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetcore.Models
{
    public class CustomerCountry : Customer
    {
        /*CustomerCountry inherits from customer
         * Stores all variables regarding CustomerCountry
         */
        public int NumberOf { get; set; }
    }
}
