﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetcore.Models
{
    public class CustomerGenre : Customer
    {
        /*CustomerGenre inherits from customer
         * Stores all variables regarding CustomerGenre
         */
        public string Name { get; set; }
    }
}
